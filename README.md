cathetus
========

The 1999 revision of the C language mandated the `hypot` function,
which calculates the hypotenuse of a right triangle given the other
sides (the _cathetii_), but avoiding the under/overflow associated
with the naive implementation.

This is the companion to `hypot`, given the hypotenuse and one side
of a right triangle, calculate the other side.
