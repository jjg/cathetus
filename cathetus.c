/*
  cathetus.c

  J.J. Green 2018, 2019
*/

#include <math.h>
#include <float.h>

#include "cathetus.h"


static double cathetus_finite(double h, double a)
{
  if (h > sqrt(DBL_MAX))
    {
      if (h > DBL_MAX / 2)
        return 2 * sqrt(h - a) * sqrt(h / 4 + a / 4);
      else
        return sqrt(h - a) * sqrt(h + a);
    }

  if (h < sqrt(DBL_MIN))
    return sqrt(h - a) * sqrt(h + a);

  return sqrt((h - a) * (h + a));
}

double cathetus(double h, double a)
{
  if (isnan(h))
    return NAN;

  if (isinf(h))
    {
      if (isinf(a))
        return NAN;
      else
        return INFINITY;
    }

  if (isnan(a))
    return NAN;

  h = fabs(h);
  a = fabs(a);

  if (h < a)
    return NAN;

  return fmin(h, cathetus_finite(h, a));
}
