/*
  cathetus.h

  J.J. Green 2018
*/

#ifndef CATHETUS_H
#define CATHETUS_H

#ifdef __cplusplus
extern "C"
{
#endif

double cathetus(double, double);

#ifdef __cplusplus
}
#endif

#endif
