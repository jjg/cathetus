cathetus documentation
--------------------

The docbook source in `cathetus.xml` is used to generate the Unix
man-page `cathetus.3` and the plain text `cathetus.txt`. The tools
`xsltproc` and `lynx` are required to regenerate these files.

The script `cathetus-fetch.sh` pulls the latest version of `cathetus.c`
and `cathetus.h` from the GitHub master to the current directory.
