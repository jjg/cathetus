/*
  gcc_version.h
  Copyright (c) J.J. Green 2018
*/

#ifndef GCC_VERSION_H
#define GCC_VERSION_H

#ifdef __GNUC__
#define GCCVER ( __GNUC__ * 10000 + \
                 __GNUC_MINOR__ * 100 +         \
                 __GNUC_PATCHLEVEL__)
#define GCC_BETWEEN(A, B) ((A <= GCCVER) && (GCCVER < B))
#else
#define GCC_BETWEEN(A, B) 0
#endif

#endif
