/*
  tests_cathetus.h

  Copyright (c) J.J. Green 2018
*/

#ifndef TESTS_CATHETUS_H
#define TESTS_CATHETUS_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_cathetus[];

void test_cathetus_reflexive(void);
void test_cathetus_triples(void);
void test_cathetus_negative(void);
void test_cathetus_zero(void);
void test_cathetus_infeasible(void);
void test_cathetus_inf_hypot(void);
void test_cathetus_nan_hypot(void);
void test_cathetus_simple_overflow(void);
void test_cathetus_simple_underflow(void);
void test_cathetus_denormal_underflow(void);
void test_cathetus_hypothesis_regression(void);

#endif
